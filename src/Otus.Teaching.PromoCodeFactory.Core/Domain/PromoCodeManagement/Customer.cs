﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public List<CustomerPreference> Preferences { get; set; } = new List<CustomerPreference>();

        public List<PromoCode> PromoCodes { get; set; } = new List<PromoCode>();
    }
}