﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предподчтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferencesController(IMapper mapper, IRepository<Preference> preferencesRepository)
        {
            _preferenceRepository = preferencesRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить список всех предподчтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<PreferenceResponse>>> GetCustomersAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var response = _mapper.Map<IEnumerable<PreferenceResponse>>(preferences);

            return Ok(response);
        }
    }
}
