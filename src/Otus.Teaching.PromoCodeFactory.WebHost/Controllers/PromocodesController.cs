﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromocodesController(
            IMapper mapper,
            IRepository<PromoCode> promocodeRepository,
            IRepository<Employee> employeeRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<Customer> customerRepository
            )
        {
            _mapper = mapper;

            _promocodeRepository = promocodeRepository;
            _employeeRepository = employeeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();
            var response = _mapper.Map<IEnumerable<PromoCodeShortResponse>>(promocodes);

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            if (!IsValidDateTime(request.BeginDate) || !IsValidDateTime(request.EndDate))
                return BadRequest();

            var employee = await _employeeRepository.GetByIdAsync(request.PartnerManagerId);
            if (employee == null)
                return BadRequest();

            var prefence = await _preferenceRepository.GetFirstAsync(p => 
                p.Name.Equals(request.Preference.Trim()));
            if (prefence == null)
                return BadRequest();

            var customers = (await _customerRepository.GetAllAsync(c => 
                c.Preferences.Any(p => p.PreferenceId == prefence.Id)))
                .ToList();

            if (!customers.Any())
                return BadRequest();
            
            var selectedCustomer = customers[new Random().Next(0, customers.Count - 1)];

            var promoCode = _mapper.Map<PromoCode>(request);
            promoCode.PartnerManager = employee;
            promoCode.Preference = prefence;
            promoCode.Customer = selectedCustomer;

            var promoCodeId = await _promocodeRepository.AddAsync(promoCode);
            return Ok(promoCodeId);
        }

        private bool IsValidDateTime(string strDateTime)
        {
            return DateTime.TryParse(strDateTime, out _);
        }
    }
}