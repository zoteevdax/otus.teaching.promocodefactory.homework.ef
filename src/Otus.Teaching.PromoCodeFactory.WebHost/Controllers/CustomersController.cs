﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(
            IMapper mapper, 
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _mapper = mapper;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }


        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = _mapper.Map<IEnumerable<CustomerShortResponse>>(customers);

            return Ok(response);
        }


        /// <summary>
        /// Получить клиента по id вместе с предподчтениями
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            var response = _mapper.Map<CustomerResponse>(customer);
            return Ok(response);
        }

        /// <summary>
        /// Создать нового клиента вместе с его предподчтениями
        /// </summary>
        /// <returns>id нового клиента</returns>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (!await IsValidPreferencesAsync(request.PreferenceIds))
            {
                return BadRequest();
            }
            
            var newCustomer = _mapper.Map<Customer>(request);
            foreach (var preferenceId in request.PreferenceIds)
            {
                newCustomer.Preferences.Add(
                    new CustomerPreference
                    {
                        PreferenceId = preferenceId
                    });
            }

            var customerId = await _customerRepository.AddAsync(newCustomer);
            return Ok(customerId);
        }

        /// <summary>
        /// Изменить клиента и его предподчтения
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (!await _customerRepository.IsExistAsync(id) || 
                !await IsValidPreferencesAsync(request.PreferenceIds))
            {
                return BadRequest();
            }

            var customerToUpdate = _mapper.Map<Customer>(request);
            customerToUpdate.Id = id;

            foreach (var preferenceId in request.PreferenceIds)
            {
                customerToUpdate.Preferences.Add(
                    new CustomerPreference
                    {
                        PreferenceId = preferenceId
                    });
            }

            await _customerRepository.UpdateAsync(customerToUpdate);
            return Ok();
        }

        /// <summary>
        /// Удалить клиента вместе с выданными ему промокодами
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            if (!await _customerRepository.IsExistAsync(id))
            {
                return BadRequest();
            }

            await _customerRepository.DeleteAsync(id);
            return Ok();
        }

        private async Task<bool> IsValidPreferencesAsync(IEnumerable<Guid> preferenceIds)
        {
            if (preferenceIds.Any())
            {
                var allPreferences = (await _preferenceRepository.GetAllAsync())
                    .Select(p => p.Id)
                    .ToHashSet();

                if (preferenceIds.Any(p => !allPreferences.Contains(p)))
                {
                    return false;
                }
            }

            return true;
        }
    }
}