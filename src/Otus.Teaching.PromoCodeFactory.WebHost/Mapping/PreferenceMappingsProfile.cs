﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class PreferenceMappingsProfile: Profile
    {
        public PreferenceMappingsProfile()
        {
            CreateMap<Preference, PreferenceResponse>();
        }
    }
}
