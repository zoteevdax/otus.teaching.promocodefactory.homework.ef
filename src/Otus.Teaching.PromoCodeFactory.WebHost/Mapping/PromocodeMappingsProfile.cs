﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class PromocodeMappingsProfile: Profile
    {
        public PromocodeMappingsProfile()
        {
            CreateMap<string, DateTime>().ConvertUsing(s => DateTime.Parse(s));

            CreateMap<PromoCode, PromoCodeShortResponse>();

            CreateMap<GivePromoCodeRequest, PromoCode>()
                .ForMember(d => d.Id, o => o.Ignore())
                .ForMember(d => d.Code, o => o.MapFrom(s => s.PromoCode))
                .ForMember(d => d.PartnerManager, o => o.Ignore())
                .ForMember(d => d.Preference, o => o.Ignore())
                .ForMember(d => d.Customer, o => o.Ignore());
                
        }
    }
}
