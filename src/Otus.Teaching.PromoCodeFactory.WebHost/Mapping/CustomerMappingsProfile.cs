﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class CustomerMappingsProfile: Profile
    {
        public CustomerMappingsProfile()
        {
            CreateMap<Customer, CustomerShortResponse>();

            CreateMap<CustomerPreference, PreferenceResponse>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(v => v.Preference.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(v => v.Preference.Name));

            CreateMap<Customer, CustomerResponse>();

            CreateMap<CreateOrEditCustomerRequest, Customer>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Preferences, opt => opt.Ignore())
                .ForMember(dest => dest.PromoCodes, opt => opt.Ignore());
        }
    }
}
