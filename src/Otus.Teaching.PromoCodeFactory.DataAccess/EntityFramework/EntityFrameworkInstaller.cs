﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework.Initialization;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework
{
    public static class EntityFrameworkInstaller
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
            string connectionString)
        {
            services.AddDbContext<PromoCodeFactoryContext>(optionsBuilder
                => optionsBuilder.UseSqlite(connectionString));

            services.AddScoped<IDbInitializer, ApplyDatabaseMigrations>();
                    
            services.AddHealthChecks()
                .AddDbContextCheck<PromoCodeFactoryContext>();

            return services;
        }
    }
}
