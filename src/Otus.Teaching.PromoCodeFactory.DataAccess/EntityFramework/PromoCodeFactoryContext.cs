﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework
{
    public class PromoCodeFactoryContext: DbContext
    {
        public static readonly ILoggerFactory DbLoggerFactory = LoggerFactory.Create(builder =>
        {
            builder.AddFilter((category, level) => category == DbLoggerCategory.Database.Command.Name
                        && level == LogLevel.Information)
                   .AddConsole();
        });

        public PromoCodeFactoryContext(DbContextOptions<PromoCodeFactoryContext> options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(cp => new {cp.CustomerId, cp.PreferenceId});

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.Preferences);

            modelBuilder.Entity<Employee>().Property(e => e.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(e => e.LastName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(e => e.Email).HasMaxLength(100);
            
            modelBuilder.Entity<Role>().Property(r => r.Name).HasMaxLength(20);
            modelBuilder.Entity<Role>().Property(r => r.Description).HasMaxLength(100);
            
            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(100);

            modelBuilder.Entity<Preference>().Property(p => p.Name).HasMaxLength(50);

            modelBuilder.Entity<PromoCode>().Property(pm => pm.Code).HasMaxLength(100);
            modelBuilder.Entity<PromoCode>().Property(pm => pm.ServiceInfo).HasMaxLength(200);
            modelBuilder.Entity<PromoCode>().Property(pm => pm.PartnerName).HasMaxLength(100);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(DbLoggerFactory);
        }
    }
}
