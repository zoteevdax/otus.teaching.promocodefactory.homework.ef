﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework.Initialization
{
    public class DbInitializer: IDbInitializer
    {
        protected readonly PromoCodeFactoryContext Context;

        public DbInitializer(PromoCodeFactoryContext context)
        {
            Context = context;
        }

        public virtual void Initialize()
        {
            Seed();
        }

        protected virtual void Seed()
        {
            Context.AddRange(FakeDataFactory.Roles);
            Context.AddRange(FakeDataFactory.Preferences);

            foreach (var employee in FakeDataFactory.Employees)
                Context.Entry(employee).State = EntityState.Added;

            foreach (var customer in FakeDataFactory.Customers)
            {
                Context.Entry(customer).State = EntityState.Added;
                customer.Preferences
                    .ForEach(p => Context.Entry(p).State = EntityState.Added);
            }
                
            Context.SaveChanges();
        }
    }
}
