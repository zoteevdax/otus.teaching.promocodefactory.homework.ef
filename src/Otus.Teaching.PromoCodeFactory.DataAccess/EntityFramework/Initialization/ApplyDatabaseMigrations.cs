﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework.Initialization
{
    public class ApplyDatabaseMigrations : DbInitializer
    {
        private readonly bool _databaseExist;

        public ApplyDatabaseMigrations(PromoCodeFactoryContext context) : base(context)
        {
            _databaseExist = (Context.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists();
        }

        public override void Initialize()
        {
            
            Context.Database.Migrate();

            if (!_databaseExist)
                base.Initialize();
        }
    }
}
