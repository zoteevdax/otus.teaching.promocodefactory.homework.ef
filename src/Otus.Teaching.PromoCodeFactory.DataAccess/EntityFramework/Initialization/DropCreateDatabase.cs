﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework.Initialization
{
    public class DropCreateDatabase: DbInitializer
    {
        public DropCreateDatabase(PromoCodeFactoryContext context): base(context) { }

        public override void Initialize()
        {
            Context.Database.EnsureDeleted();
            Context.Database.EnsureCreated();

            base.Initialize();
        }
    }
}
