﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework.Initialization
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
