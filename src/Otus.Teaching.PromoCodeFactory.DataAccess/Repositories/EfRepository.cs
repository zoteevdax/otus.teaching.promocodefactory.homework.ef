﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly PromoCodeFactoryContext Context;
        protected readonly DbSet<T> Entities;

        public EfRepository(PromoCodeFactoryContext context)
        {
            Context = context;
            Entities = Context.Set<T>();
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Entities.ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>> predicate)
        {
            return await Entities.Where(predicate).ToListAsync();
        }

        public virtual async Task<T> GetFirstAsync(Expression<Func<T, bool>> predicate)
        {
            return await Entities.FirstOrDefaultAsync(predicate);
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await Entities.FindAsync(id);
        }

        public async Task<bool> IsExistAsync(Guid id)
        {
            return await Entities.AnyAsync(x => x.Id == id);
        }

        public virtual async Task<Guid> AddAsync(T entity)
        {
            Context.Attach(entity).State = EntityState.Added;
            await Context.SaveChangesAsync();

            return entity.Id;
        }

        public virtual async Task UpdateAsync(T entity)
        {
            Entities.Update(entity);
            await Context.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(Guid id)
        {
            var existEntity = await Entities.FindAsync(id);
            if (existEntity != null)
            {
                Entities.Remove(existEntity);
                await Context.SaveChangesAsync();
            }
        }
    }
}
