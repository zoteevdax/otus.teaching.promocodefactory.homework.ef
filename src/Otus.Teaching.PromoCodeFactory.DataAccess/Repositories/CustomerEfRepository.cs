﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerEfRepository : EfRepository<Customer>
    {
        public CustomerEfRepository(PromoCodeFactoryContext context) : base(context)
        {
        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            return await Entities
                .Include(c => c.PromoCodes)
                .Include(c => c.Preferences)
                    .ThenInclude(p => p.Preference)
                .FirstOrDefaultAsync(c => c.Id == id);                   
        }

        public override async Task UpdateAsync(Customer entity)
        {
            var existingCustomer = await GetByIdAsync(entity.Id);     
            Context.Entry(existingCustomer).CurrentValues.SetValues(entity);

            existingCustomer.Preferences = entity.Preferences;

            await Context.SaveChangesAsync();
        }

        public async override Task DeleteAsync(Guid id)
        {
            var cutomerToDelete = await Entities
                .Include(c => c.PromoCodes)
                .FirstOrDefaultAsync(c => c.Id == id);

            foreach (var promoCode in cutomerToDelete.PromoCodes)
                Context.Remove(promoCode);

            Context.Remove(cutomerToDelete);

            await Context.SaveChangesAsync();
        }
    }
}
